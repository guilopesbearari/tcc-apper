// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const registroAmbiental = sequelizeClient.define('registro_ambiental', {
    nomeMonitor: {
      type: DataTypes.STRING,
      allowNull: false
    },
    local: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    latitude: {
      type: DataTypes.STRING,
      allowNull: false
    },
    longitude: {
      type: DataTypes.STRING,
      allowNull: false
    },
    dataAcao: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    objetivos: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    setor: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    funcionariosPresentes: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    descricao: {
      type: DataTypes.STRING(1000),
      allowNull: false
    },
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  registroAmbiental.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return registroAmbiental;
};
