const users = require('./users/users.service.js');
const registroAmbiental = require('./registro-ambiental/registro-ambiental.service.js');
const registroVisita = require('./registro-visita/registro-visita.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(registroAmbiental);
  app.configure(registroVisita);
};
