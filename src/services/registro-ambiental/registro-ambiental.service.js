// Initializes the `registro-ambiental` service on path `/registro-ambiental`
const { RegistroAmbiental } = require('./registro-ambiental.class');
const createModel = require('../../models/registro-ambiental.model');
const hooks = require('./registro-ambiental.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/registro-ambiental', new RegistroAmbiental(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('registro-ambiental');

  service.hooks(hooks);
};
