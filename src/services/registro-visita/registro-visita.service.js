// Initializes the `registro-visita` service on path `/registro-visita`
const { RegistroVisita } = require('./registro-visita.class');
const createModel = require('../../models/registro-visita.model');
const hooks = require('./registro-visita.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/registro-visita', new RegistroVisita(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('registro-visita');

  service.hooks(hooks);
};
