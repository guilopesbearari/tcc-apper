const Sequelize = require('sequelize');
const logger = require('./logger')

module.exports = function (app) {
  const sql = app.get('mysql');
  const sequelize = new Sequelize(sql.schema, sql.user, sql.password, {
    host: sql.url,
    port: sql.port,
    dialect: 'mysql',
    logging: false,
    define: {
      freezeTableName: true
    },
    timezone: '-03:00',
  });
  const oldSetup = app.setup;

  app.set('sequelizeClient', sequelize);

  app.setup = function (...args) {
    const result = oldSetup.apply(this, args);

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ('associate' in models[name]) {
        models[name].associate(models);
      }
    });

    // Sync to the database
    app.set('sequelizeSync', sequelize.sync());

    logger.info('[DB Backend] Connected')

    return result;
  };
};
