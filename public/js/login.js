$(document).ready(() => {
  $("#acessar").off('click').on('click', async (ev) => {
    ev.preventDefault()
    const json = {
      email: $("#email").val(),
      password: $("#password").val(),
      strategy: 'local'
    }
    login = await window.app.service('authentication').create(json)
    sessionStorage.setItem('token', login.accessToken)
    window.location.href = '/dashboard.html'
  })
})