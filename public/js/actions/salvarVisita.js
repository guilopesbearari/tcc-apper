$(document).ready(() => {
  $("#cadastrarVisita").off('click').on('click', async (ev) => {
    ev.preventDefault()
    const json = {
      	dataCadastro: $("#dataCadastro").val(),
		local: $("#local").val(),
		totalVisitantes: $("#totalVisitantes").val()
    }
    try {
    	$.ajax({
		    url: '/registro-visita',
		    headers: {
		        'Authorization':`Bearer ${sessionStorage.getItem('token')}`
		    },
		    method: 'POST',
		    dataType: 'json',
		    data: json,
		    success: function(data){
		      alert('cadastrado com sucesso')
		    },
		    error: function(erro) {
		    	alert('deu ruim')
		    	console.log('erro', erro)
		    }
	  	});    	
    } catch (e) {
    	console.log('erro', e)
    	alert('erro')
    }
  })
})