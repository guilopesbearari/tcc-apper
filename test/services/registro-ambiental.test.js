const assert = require('assert');
const app = require('../../src/app');

describe('\'registro-ambiental\' service', () => {
  it('registered the service', () => {
    const service = app.service('registro-ambiental');

    assert.ok(service, 'Registered the service');
  });
});
