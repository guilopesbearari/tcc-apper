const assert = require('assert');
const app = require('../../src/app');

describe('\'registro-visita\' service', () => {
  it('registered the service', () => {
    const service = app.service('registro-visita');

    assert.ok(service, 'Registered the service');
  });
});
