const assert = require('assert');
const app = require('../../src/app');

describe('\'funcionarios\' service', () => {
  it('registered the service', () => {
    const service = app.service('funcionarios');

    assert.ok(service, 'Registered the service');
  });
});
